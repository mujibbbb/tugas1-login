import React from 'react';
import * as FaIcons from 'react-icons/fa';
import * as AiIcons from 'react-icons/ai';
import * as IoIcons from 'react-icons/io';
import * as FiIcons from 'react-icons/fi';

export const SidebarData = [
  {
    title: 'Home',
    path: '/',
    icon: <FaIcons.FaUserGraduate />,
    cName: 'nav-text'
  },
  {
    title: 'Login',
    path: '/login',
    icon: <FiIcons.FiLogIn />,
    cName: 'nav-text'
  },
  {
    title: 'Signup',
    path: '/signup',
    icon: <FaIcons.FaSignInAlt />,
    cName: 'nav-text'
  }
];