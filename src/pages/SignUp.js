import React, { useState } from 'react';
import '../App.css';
import { Container, Form, InputGroup, Card, FormControl, Button, Spinner } from 'react-bootstrap';
import * as FaIcons from 'react-icons/fa';
import * as RiIcons from 'react-icons/ri';

function SignUp() {
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [password2, setPassword2] = useState('');
    const [invalid, setInvalid] = useState(false);
    const [invalidPwd, setInvalidPwd] = useState(false);
    const [invalidPwd2, setInvalidPwd2] = useState(false);
    const [loading, setLoading] = useState(false);
    const handleSubmit = (e) => {


        // setLoading(true);
        alert('Become to Admin ' + username + ' ' + email + ' ' + password + ' ' + password2);
        e.preventDefault();

    }

    return <div>
        <Container>
            <div className="d-flex justify-content-center mt-4">
                <Card>
                    <Card.Body><h2>Cuss Daftar <b> | Admin</b><hr></hr></h2>
                        <Form onSubmit={handleSubmit}>
                            <Form.Group>
                                <Form.Label>
                                    Username
                            </Form.Label>
                                <InputGroup>
                                    <InputGroup.Prepend>
                                        <InputGroup.Text>
                                            <FaIcons.FaUserPlus />
                                        </InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <FormControl
                                        type='username'
                                        value={username}
                                        placeholder='Username'
                                        isInvalid={invalid}
                                        onChange={(e) => setUsername(e.target.value)}
                                        onBlur={() => {
                                            if (!username.length) {
                                                setInvalid(true);
                                            }
                                        }}

                                    />

                                </InputGroup>
                            </Form.Group>
                            {/* akhir form group */}
                            <Form.Group>
                                <Form.Label>
                                    Email
                            </Form.Label>
                                <InputGroup>
                                    <InputGroup.Prepend>
                                        <InputGroup.Text>
                                            <FaIcons.FaEnvelopeOpenText />
                                        </InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <FormControl
                                        type='email'
                                        placeholder='Email Address'
                                        value={email}
                                        isInvalid={invalid}
                                        onChange={(e) => setEmail(e.target.value)}
                                        onBlur={() => {
                                            if (!email.length) {
                                                setInvalid(true);
                                            }
                                        }}

                                    />

                                </InputGroup>
                            </Form.Group>
                            {/* akhir form group */}
                            <Form.Group>
                                <Form.Label>
                                    Password
                            </Form.Label>
                                <InputGroup>
                                    <InputGroup.Prepend>
                                        <InputGroup.Text>
                                            <RiIcons.RiLockPasswordLine />
                                        </InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <FormControl
                                        type='password'
                                        placeholder='Password'
                                        value={password}
                                        isInvalid={invalidPwd}
                                        onChange={(e) => setPassword(e.target.value)}
                                        onBlur={() => {
                                            if (!password.length) {
                                                setInvalidPwd(true);
                                            }
                                        }}
                                    />
                                </InputGroup>
                            </Form.Group>
                            {/* akhir form group */}
                            <Form.Group>
                                <Form.Label>
                                    Confirm Password
                            </Form.Label>
                                <InputGroup>
                                    <InputGroup.Prepend>
                                        <InputGroup.Text>
                                            <RiIcons.RiLockPasswordLine />
                                        </InputGroup.Text>
                                    </InputGroup.Prepend>
                                    <FormControl
                                        type='password'
                                        placeholder='Password'
                                        value={password2}
                                        isInvalid={invalidPwd2}
                                        onChange={(e) => setPassword2(e.target.value)}
                                        onBlur={() => {
                                            if (!password2.length) {
                                                setInvalidPwd2(true);
                                            }
                                        }}
                                    />
                                </InputGroup>
                            </Form.Group>
                            <Button block type='submit'>
                                Register
                        </Button>

                        </Form>
                    </Card.Body>
                    {/* <FacebookLoginButton /> */}
                </Card>

            </div>
        </Container>
    </div>;
}

export default SignUp
